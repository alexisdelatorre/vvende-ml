require('dotenv').config()

let withLess = require('@zeit/next-less')

let env = process.env
delete env.NODE_ENV

module.exports = {
    ...withLess({
        lessLoaderOptions: {
            javascriptEnabled: true,
        },
    }),
    env: process.env,
}
