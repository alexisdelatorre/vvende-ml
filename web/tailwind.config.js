module.exports = {
    theme: {
        screens: {
            sm: '600px',
            md: '700px',
            lg: '800px',
            xl: '1200px',
        },
        extend: {
            colors: {
                'antd-gray-100': '#f0f0f0',
            },
        },
    },
    variants: {},
    corePlugins: {
        preflight: false,
        container: false,
    },
    plugins: [
        function({ addComponents }) {
            addComponents({
                '.container': {
                    maxWidth: '100%',
                    '@screen sm': {
                        maxWidth: '600px',
                    },
                    '@screen md': {
                        maxWidth: '700px',
                    },
                    '@screen lg': {
                        maxWidth: '800px',
                    },
                    '@screen xl': {
                        maxWidth: '1200px',
                    },
                },
            })
        },
    ],
}
