import { Result, Button } from 'antd'
import { Centered } from '../lib/utilComponents'

export default function Custom404() {
    return (
        <Centered>
            <Result
                status='404'
                title='404'
                subTitle='Sorry, the page you visited does not exist.'
                extra={<Button type='primary'>Back Home</Button>}
            />
        </Centered>
    )
}
