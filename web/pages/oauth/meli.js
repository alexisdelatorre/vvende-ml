import { Spin, Alert, Result, Typography, Button } from 'antd'
import { server } from '../../lib/endpoints'
import { useRouter } from 'next/router'
import { useEffect, useState, useContext } from 'react'
import { Context } from '../_app'
import { Centered } from '../../lib/utilComponents'

function Meli() {
    let router = useRouter()
    let { setAuthenticatedMeli } = useContext(Context)

    let { code } = router.query

    let [success, setSuccess] = useState(false)
    let [error, setError] = useState(null)

    useEffect(() => {
        if (code) {
            server.post('/oauth/meli/authenticate', { code }).then((res) => {
                if (res.status === 200) {
                    setSuccess(true)
                    setAuthenticatedMeli(true)
                    setTimeout(() => router.push('/'), 5000)
                    return
                }

                if (res.status === 400) {
                    setError(res.data)
                    return
                }
            })
        }
    }, [code])

    let successMessage =
        'The authentication was a success. You will be automatically redirected to the main page in 5 seconds.'

    return (
        <Centered>
            <div className='container mx-auto'>
                {!error && !success && (
                    <div className='flex flex-col space-y-4 text-center'>
                        <span>Authenticating with Mercado Libre</span>
                        <Spin size='large' />
                    </div>
                )}

                {!error && success && (
                    <Result
                        status='success'
                        title={`Authenticated`}
                        subTitle='You will be automatically redirected to the main page in 5 seconds.'
                        extra={<Button onClick={() => router.push('/')}>Go To Main Page</Button>}
                    />
                )}

                {error && (
                    <Result
                        status='error'
                        title={`Error: ${error.error}`}
                        extra={
                            <Button type='primary' onClick={() => router.push('/settings')}>
                                Go Back To Connection Settings
                            </Button>
                        }
                    >
                        {error.message}
                    </Result>
                )}
            </div>
        </Centered>
    )
}

export default Meli
