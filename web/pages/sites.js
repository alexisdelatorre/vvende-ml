import { useEffect, useState } from 'react'
import { server } from '../lib/endpoints'
import { Table, Typography } from 'antd'
import Link from 'next/link'

function Sites() {
    const [sites, setSites] = useState(null)
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        server.get('/meli/sites').then((res) => {
            setSites(res.data)
            setLoading(false)
        })
    }, [])

    return (
        <>
            <Table
                columns={[
                    { title: 'ID', dataIndex: 'id', key: 'id' },
                    { title: 'Name', dataIndex: 'name', key: 'name' },
                    { title: 'Currency', dataIndex: 'default_currency_id', key: 'currency' },
                    {
                        key: 'actions',
                        render: (text, record) => {
                            return (
                                <Link href={`sites/${record['id']}/listing-types`}>
                                    <a>Listing Types</a>
                                </Link>
                            )
                        },
                    },
                ]}
                dataSource={sites}
                rowKey={(record) => record['id']}
                bordered
                loading={loading}
                pagination={false}
                className='mb-20'
            />
        </>
    )
}

export default Sites
