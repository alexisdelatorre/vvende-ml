import Link from 'next/link'
import Title from 'antd/lib/typography/Title'
import { Card, Button, Input, Form, Alert } from 'antd'
import { server } from '../lib/endpoints'
import { useRouter } from 'next/router'
import { useState } from 'react'
import { Centered } from '../lib/utilComponents'

function Login() {
    let router = useRouter()
    let [loading, setLoading] = useState(false)
    let [error, setError] = useState(null)

    let handleForm = (values) => {
        setLoading(true)

        let payload = {
            grant_type: 'password',
            username: values.username,
            password: values.password,
        }

        server
            .post('/oauth/token', payload)
            .then((res) => {
                if (res.status === 200) {
                    localStorage.setItem('token', res.data?.access_token)
                    router.push('/')
                    setError(null)
                    setLoading(false)
                    return
                }

                if (res.status === 400 && res.data?.error === 'invalid_grant') {
                    setError('Invalid email or password')
                    setLoading(false)
                    return
                }

                if (res.status === 500) {
                    console.error(res)
                    setError('Server error, try again later')
                    setLoading(false)
                    return
                }

                console.error(res)
                setError('There was a unknown error')
                setLoading(false)
            })
            .catch((err) => {
                setLoading(false)
                setError(err.message)
            })
    }

    return (
        <Centered>
            <div className={'container max-w-lg'}>
                <div className={'shadow-sm'}>
                    <Card title={'Login'} style={{ minWidth: 450 }}>
                        {error && (
                            <div className={'mb-4'}>
                                <Alert type='error' message={error} />
                            </div>
                        )}
                        <Form layout='vertical' onFinish={handleForm} hideRequiredMark>
                            <Form.Item label='Email' name='username' rules={[{ required: true }]}>
                                <Input />
                            </Form.Item>

                            <Form.Item
                                label='Password'
                                name='password'
                                rules={[{ required: true }]}
                            >
                                <Input.Password />
                            </Form.Item>
                            <Form.Item>
                                <Button
                                    type={'primary'}
                                    className={'mt-2'}
                                    htmlType='submit'
                                    loading={loading}
                                    block
                                >
                                    Continue
                                </Button>
                                <Link href='/registration'>
                                    <span>
                                        <Button type={'link'} block>
                                            Or Register
                                        </Button>
                                    </span>
                                </Link>
                            </Form.Item>
                        </Form>
                    </Card>
                </div>
            </div>
        </Centered>
    )
}

export default Login
