import '../styles.less'

import { useRouter } from 'next/router'
import { Menu, Badge, Dropdown, message } from 'antd'
import { useState, useEffect } from 'react'
import { server } from '../lib/endpoints'
import { AccountCircleIcon, SettingsIcon } from '../lib/icons'

export const Context = React.createContext()

function Header({ onUserMenuClick, userDropdown }) {
    return (
        <header className='flex items-center justify-between pr-4 py-4 shadow-md z-50'>
            <div className={'font-semibold border-rr pl-4'} style={{ width: 160 }}>
                Vende ML
            </div>
            {userDropdown && (
                <Dropdown
                    overlay={
                        <Menu onClick={onUserMenuClick}>
                            <Menu.Item key='logout'>Log Out</Menu.Item>
                        </Menu>
                    }
                    placement='bottomRight'
                    trigger={['click']}
                >
                    <AccountCircleIcon className={'text-2xl'} />
                </Dropdown>
            )}
        </header>
    )
}

function Sidebar({ onSelect, selected }) {
    return (
        <aside className='mt-4'>
            <Menu
                style={{ width: 160, height: '100%' }}
                selectedKeys={[selected]}
                onSelect={onSelect}
            >
                <Menu.Item key={'user'}>User</Menu.Item>
                <Menu.Item key={'sites'}>Sites</Menu.Item>
                <Menu.Item key={'settings'}>Settings</Menu.Item>
            </Menu>
        </aside>
    )
}

export default function MyApp({ Component, pageProps }) {
    let router = useRouter()

    let [user, setUser] = useState(null)
    let [loadingUser, setLoadingUser] = useState(true)
    let [authenticatedMeli, setAuthenticatedMeli] = useState(false)
    let [currentSelectedMenu, setCurrentSelectedMenu] = useState(null)

    useEffect(() => {
        if (router.pathname === '/') router.push('/user')
    }, [router.pathname])

    let isPublicPage = ['/login', '/registration'].includes(router.pathname)

    useEffect(() => {
        if (isPublicPage && localStorage.getItem('token')) router.push('/')
    })

    useEffect(() => {
        if (!loadingUser && isPublicPage && user !== null) router.push('/')
        if (!loadingUser && !isPublicPage && user === null) router.push('/login')
    }, [user])

    useEffect(() => {
        server
            .get('/users/me')
            .then((res) => {
                if (res.status === 200) {
                    console.log('got user ', res.data)
                    setUser(res.data.user)
                    setAuthenticatedMeli(res.data.authenticated_meli)
                    setLoadingUser(false)
                    return
                }

                if (res.status === 401) {
                    setUser(null)
                    localStorage.removeItem('token')
                    setLoadingUser(false)
                    router.push('/login')
                    return
                }
            })
            .catch((e) => {
                message.error({
                    content: (
                        <>
                            There was an error communicating with the server, please{' '}
                            <a onClick={() => window.location.reload(false)} className='font-bold'>
                                reload the page
                            </a>
                        </>
                    ),
                    duration: 0,
                })

                console.error(e)
            })
    }, [])

    useEffect(() => {
        if (router.pathname === '/') setCurrentSelectedMenu('/user')
        else setCurrentSelectedMenu(router.pathname.substr(1))
    }, [router.pathname])

    let handleMenuSelect = ({ key }) => router.push('/' + key)

    let handleUserMenuClick = ({ key }) => {
        if (key === 'logout') {
            localStorage.removeItem('token')
            router.push('/login')
        }
    }

    return (
        <Context.Provider value={{ user, authenticatedMeli, setAuthenticatedMeli }}>
            <div className={'flex flex-col h-screen'}>
                <Header onUserMenuClick={handleUserMenuClick} userDropdown={!isPublicPage} />
                <div className={'flex-1 flex overflow-y-hidden'}>
                    {!isPublicPage && (
                        <>
                            <Sidebar onSelect={handleMenuSelect} selected={currentSelectedMenu} />
                            <div className={'flex-1 h-full overflow-scroll'}>
                                <div className='container mx-auto h-full py-10'>
                                    <Component {...pageProps} />
                                </div>
                            </div>
                        </>
                    )}

                    {isPublicPage && (
                        <div className={'flex-1 h-full'}>
                            <Component {...pageProps} />
                        </div>
                    )}
                </div>
            </div>
        </Context.Provider>
    )
}
