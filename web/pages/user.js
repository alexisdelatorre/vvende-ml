import { Typography, Descriptions, Alert, Tag, Result, Button } from 'antd'
import { useEffect, useState } from 'react'
import { server } from '../lib/endpoints'
import { LinkIcon } from '../lib/icons'
import { useRouter } from 'next/router'
import { Centered } from '../lib/utilComponents'

function User() {
    let router = useRouter()

    let [meliUser, setMeliUser] = useState(null)
    let [error, setError] = useState(null)
    let [noMeliAcctConected, setNoMeliAcctConected] = useState(false)

    useEffect(() => {
        server.get('/meli/users/me').then((res) => {
            if (res.status === 200) setMeliUser(res.data)
            if (res.status === 401) setNoMeliAcctConected(true)
            if (res.status === 500) setError(res.data)
        })
    }, [])

    if (noMeliAcctConected) {
        return (
            <Centered>
                <Result
                    status='warning'
                    title={'You need to connect your Mercado Libre account'}
                    subTitle='To access this page you first need to connect your account'
                    extra={
                        <Button type='primary' onClick={() => router.push('/settings')}>
                            Go To Connection Settings
                        </Button>
                    }
                />
            </Centered>
        )
    }

    if (error !== null) {
        return (
            <Centered>
                <Result
                    status='error'
                    title={`Error: ${error.error}`}
                    subTitle='There was an error getting User information from Mercado Libre'
                >
                    {error.message}
                </Result>
            </Centered>
        )
    }

    return (
        <>
            {meliUser !== null && (
                <>
                    <Typography>
                        <Typography.Title level={2} style={{ margin: 0 }}>
                            {meliUser.first_name} {meliUser.last_name}
                        </Typography.Title>
                        <Typography.Paragraph style={{ margin: 0 }}>
                            {meliUser.nickname}{' '}
                            <a href={meliUser.permalink}>
                                <LinkIcon className={'text-xl'} />
                            </a>
                        </Typography.Paragraph>
                    </Typography>

                    <Descriptions className={'mt-4'} bordered size='small'>
                        <Descriptions.Item label='Email' span={2}>
                            {meliUser.email}
                        </Descriptions.Item>
                        <Descriptions.Item label='Phone'>
                            <div className='flex space-x-2'>
                                <span>{meliUser.phone.number}</span>
                                {meliUser.phone.verified ? (
                                    <Tag color='success'>verified</Tag>
                                ) : (
                                    <Tag color='error'>not verified</Tag>
                                )}
                            </div>
                        </Descriptions.Item>
                        <Descriptions.Item label='Country'>{meliUser.country_id}</Descriptions.Item>
                        <Descriptions.Item label='User Type'>
                            {meliUser.user_type}
                        </Descriptions.Item>
                        <Descriptions.Item label='Seller Experience'>
                            {meliUser.seller_experience}
                        </Descriptions.Item>
                        <Descriptions.Item label='Registration' span={2}>
                            {meliUser.registration_date}
                        </Descriptions.Item>
                        <Descriptions.Item label='Site ID'>{meliUser.site_id}</Descriptions.Item>
                        <Descriptions.Item label='Tags' span={3}>
                            <ul className='flex p-0 m-0 list-none'>
                                {meliUser.tags.map((tag, i) => (
                                    <li key={i}>
                                        <Tag>{tag}</Tag>
                                    </li>
                                ))}
                            </ul>
                        </Descriptions.Item>
                        <Descriptions.Item label='Internal Tags' span={3}>
                            <ul className='flex p-0 m-0 list-none'>
                                {meliUser.internal_tags.map((tag, i) => (
                                    <li key={i}>
                                        <Tag>{tag}</Tag>
                                    </li>
                                ))}
                            </ul>
                        </Descriptions.Item>
                        <Descriptions.Item label='Address' span={2}>
                            {meliUser.address.address || 'Not Defined'}
                        </Descriptions.Item>
                        <Descriptions.Item label='City' span={1}>
                            {meliUser.address.city || 'Not Defined'}
                        </Descriptions.Item>
                        <Descriptions.Item label='State' span={2}>
                            {meliUser.address.state || 'Not Defined'}
                        </Descriptions.Item>
                        <Descriptions.Item label='Zip Code' span={1}>
                            {meliUser.address.zip_code || 'Not Defined'}
                        </Descriptions.Item>
                        <Descriptions.Item label='Points'>{meliUser.points}</Descriptions.Item>
                        <Descriptions.Item label='Credit Consumed'>
                            {meliUser.credit.consumed}
                        </Descriptions.Item>
                        <Descriptions.Item label='Credit Level'>
                            {meliUser.credit.credit_level_id}
                        </Descriptions.Item>
                        <Descriptions.Item label='Credit Rank'>
                            {meliUser.credit.rank}
                        </Descriptions.Item>
                    </Descriptions>
                </>
            )}
        </>
    )
}

export default User
