import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
import { server } from '../../../lib/endpoints'
import {
    Breadcrumb,
    Card,
    Divider,
    List,
    Space,
    Typography,
    Table,
    Tag,
    Collapse,
    Skeleton,
    Spin,
} from 'antd'
import Link from 'next/link'
import Title from 'antd/lib/skeleton/Title'
import { CheckIcon, CloseIcon } from '../../../lib/icons'
import { formatMoney } from 'accounting-js'

function ListingTypes() {
    let router = useRouter()

    let [siteName, setSiteName] = useState(null)
    let [listingTypes, setListingTypes] = useState([])
    let [loading, setLoading] = useState(true)

    let { siteId } = router.query

    let parseConf = (c) => ({
        exposure: c['listing_exposure'],
        buyItNow: c['duration_days']['buy_it_now'],
        auction: c['duration_days']['auction'],
        classified: c['duration_days']['classified'],
        immediatePaymentBuyItNow: c['immediate_payment']['buy_it_now'],
        immediatePaymentAuction: c['immediate_payment']['auction'],
        immediatePaymentClassified: c['immediate_payment']['classified'],
        mercadoPago: c['mercado_pago'],
        saleCurrency: c['sale_fee_criteria']['currency'],
        listingCurrency: c['listing_fee_criteria']['currency'],
        saleMax: c['sale_fee_criteria']['max_fee_amount'],
        listingMax: c['listing_fee_criteria']['max_fee_amount'],
        saleMin: c['sale_fee_criteria']['min_fee_amount'],
        listingMin: c['listing_fee_criteria']['min_fee_amount'],
        salePercentage: c['sale_fee_criteria']['percentage_of_fee_amount'],
        listingPercentage: c['listing_fee_criteria']['percentage_of_fee_amount'],
        requiresPicture: c['requires_picture'],
    })

    useEffect(() => {
        if (siteId) {
            ;(async () => {
                let site = (await server.get(`/meli/sites/${siteId}`)).data
                setSiteName(site.name)

                let listingTypes = (await server.get(`/meli/sites/${siteId}/listing_types`)).data

                let acc = []

                for (let listingType of listingTypes) {
                    let details = (
                        await server.get(`/meli/sites/${siteId}/listing_types/${listingType['id']}`)
                    ).data
                    console.log({ listingType, details })

                    let exceptions = details['exceptions_by_category']
                    let exceptionsAcc = []

                    for (let exception of exceptions) {
                        exceptionsAcc.push({
                            category: exception['category_name'],
                            ...parseConf(exception['configuration']),
                        })
                    }

                    acc.push({
                        id: listingType['id'],
                        name: listingType['name'],
                        configuration: [
                            ...exceptionsAcc,
                            {
                                category: exceptions.length === 0 ? 'All Categories' : 'All Else',
                                ...parseConf(details['configuration']),
                            },
                        ],
                    })
                }
                setListingTypes(acc)
                setLoading(false)
            })()
        }
    }, [siteId])

    console.log(listingTypes)

    return (
        <>
            <div className='space-y-2'>
                <Breadcrumb>
                    <Breadcrumb.Item>
                        <Link href='/sites'>
                            <a>Sites</a>
                        </Link>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>Listing Types</Breadcrumb.Item>
                    <Breadcrumb.Item>{siteId}</Breadcrumb.Item>
                </Breadcrumb>
                <Typography>
                    <Typography.Title level={2} style={{ margin: 0 }}>
                        Listing Types
                    </Typography.Title>
                    {!loading ? (
                        <Typography.Title level={3} style={{ margin: 0 }}>
                            {siteName}
                        </Typography.Title>
                    ) : (
                        <Skeleton paragraph={{ rows: 0 }} loading style={{ margin: 0 }}></Skeleton>
                    )}
                </Typography>
            </div>
            <Divider></Divider>
            {!loading ? (
                <Collapse accordion>
                    {listingTypes.map((lt) => (
                        <Collapse.Panel header={lt.name} key={lt.id}>
                            <div className='space-y-4'>
                                {lt.configuration.map((c) => (
                                    <Card>
                                        <div className='flex justify-between'>
                                            <Typography.Title level={4} style={{ margin: 0 }}>
                                                {c.category}
                                            </Typography.Title>
                                            <div>
                                                <Tag>Exposure: {c.exposure}</Tag>
                                                <Tag>Mercado Pago: {c.mercadoPago}</Tag>
                                                {c.requiresPicture && <Tag>Requires Picture</Tag>}
                                            </div>
                                        </div>
                                        <Divider></Divider>
                                        <Typography.Paragraph>
                                            <Typography.Text strong>Buy It Now: </Typography.Text>
                                            {!c.buyItNow && <>Not Available</>}
                                            {c.buyItNow && (
                                                <>
                                                    {c.buyItNow} Days Duration &middot;{' '}
                                                    {c.immediatePaymentBuyItNow
                                                        ? 'Immediate payment'
                                                        : 'No immediate payment'}
                                                </>
                                            )}
                                        </Typography.Paragraph>
                                        <Typography.Paragraph>
                                            <Typography.Text strong>Auction: </Typography.Text>
                                            {!c.auction && <>Not Available</>}
                                            {c.auction && (
                                                <>
                                                    {c.auction} Days Duration &middot;{' '}
                                                    {c.immediatePaymentAuction
                                                        ? 'Immediate payment'
                                                        : 'No immediate payment'}
                                                </>
                                            )}
                                        </Typography.Paragraph>
                                        <Typography.Paragraph>
                                            <Typography.Text strong>Classified: </Typography.Text>
                                            {!c.classified && <>Not Available</>}
                                            {c.classified && (
                                                <>
                                                    {c.classified} Days Duration &middot;{' '}
                                                    {c.immediatePaymentClassified
                                                        ? 'Immediate payment'
                                                        : 'No immediate payment'}
                                                </>
                                            )}
                                        </Typography.Paragraph>
                                        <Divider orientation='left'>Fees</Divider>
                                        <Typography.Paragraph>
                                            <Typography.Text strong className='mr-2'>
                                                Maximum:
                                            </Typography.Text>{' '}
                                            {formatMoney(c.saleMax)} {c.saleCurrency} on Sales{' '}
                                            <Divider type='vertical'></Divider>
                                            {formatMoney(c.listingMax)} {c.listingCurrency} on
                                            Listings
                                        </Typography.Paragraph>
                                        <Typography.Paragraph>
                                            <Typography.Text strong className='mr-2'>
                                                Minimum:
                                            </Typography.Text>{' '}
                                            {formatMoney(c.saleMin)} {c.saleCurrency} on Sales{' '}
                                            <Divider type='vertical'></Divider>
                                            {formatMoney(c.listingMin)} {c.listingCurrency} on
                                            Listings
                                        </Typography.Paragraph>
                                        <Typography.Paragraph>
                                            <Typography.Text strong className='mr-2'>
                                                Percentage:
                                            </Typography.Text>{' '}
                                            {c.salePercentage}% on Sales{' '}
                                            <Divider type='vertical'></Divider>
                                            {c.listingPercentage}% on Listings
                                        </Typography.Paragraph>
                                    </Card>
                                ))}
                            </div>
                        </Collapse.Panel>
                    ))}
                </Collapse>
            ) : (
                <div className='flex justify-center items-center'>
                    <Spin></Spin>
                </div>
            )}
        </>
    )
}

export default ListingTypes
