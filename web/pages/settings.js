import { Typography, Button, Divider, Popconfirm } from 'antd'
import { LinkIcon, LinkOffIcon } from '../lib/icons'
import { server } from '../lib/endpoints'

import { Context } from './_app'
import { useContext } from 'react'

function Settings() {
    let { authenticatedMeli, setAuthenticatedMeli } = useContext(Context)

    let connect = () => {
        server.get('/oauth/meli/url').then((res) => {
            if (res.status === 200) window.open(res.data, '_self')
        })
    }

    let disconnect = () => {
        server.post('/oauth/meli/disconnect').then((res) => {
            if (res.status === 200) setAuthenticatedMeli(false)
        })
    }

    return (
        <>
            <Typography>
                <Typography.Title level={3}>Connect with Mercado Libre</Typography.Title>
                {!authenticatedMeli && (
                    <>
                        <Typography.Paragraph>
                            Click on the button below to authenticate and connect your Mercado Libre
                            account to your Vende-ML account
                        </Typography.Paragraph>
                        <Button icon={<LinkIcon />} onClick={connect}>
                            Connect
                        </Button>
                    </>
                )}
                {authenticatedMeli && (
                    <>
                        <Typography.Paragraph>
                            You already have a conected Mercado Libre Account, if you want to
                            connect a different one please disconnect first
                        </Typography.Paragraph>
                        <Popconfirm
                            title='Disconnect Mercado Libre account?'
                            onConfirm={disconnect}
                        >
                            <Button icon={<LinkOffIcon />}>Disconnect</Button>
                        </Popconfirm>
                    </>
                )}
            </Typography>
            <Divider />
        </>
    )
}

export default Settings
