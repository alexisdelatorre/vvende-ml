class CreateMeliOauthTokens < ActiveRecord::Migration[6.0]
  def change
    create_table :meli_oauth_tokens do |t|
      t.references :user
      t.string :access_token
      t.string :expires_in
      t.string :scope
      t.string :meli_user_id
      t.string :refresh_token

      t.timestamps
    end
  end
end
