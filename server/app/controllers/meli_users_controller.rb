require 'net/http'

class MeliUsersController < ApplicationController
  before_action :doorkeeper_authorize!

  def me
    if current_user.meli_oauth_token.nil?
      render status: 401
      return
    end

    url = "https://api.mercadolibre.com/users/me?access_token=#{current_user.meli_oauth_token.access_token}"
    res = Net::HTTP.get(URI::parse(url))
    parsed = JSON.parse(res)

    if parsed.key?('error') && parsed['status'] != 200
      payload = { error: parsed['error'], message: parsed['message'], cause: parsed['cause'] }
      render status: :internal_server_error, json: payload
    else
      render json: parsed
    end
  end
end
