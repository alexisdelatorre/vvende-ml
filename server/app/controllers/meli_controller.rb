require 'net/http'

class MeliController < ApplicationController
  def sites
    render json: Net::HTTP.get(URI::parse("https://api.mercadolibre.com/sites"))
  end

  def site
    render json: Net::HTTP.get(URI::parse("https://api.mercadolibre.com/sites/#{params['site_id']}"))
  end

  def listing_types
    url = "https://api.mercadolibre.com/sites/#{params['site_id']}/listing_types"
    res = Net::HTTP.get(URI::parse(url))
    render json: res
  end

  def listing_type
    url = "https://api.mercadolibre.com/sites/#{params['site_id']}/listing_types/#{params['listing_type_id']}"
    res = Net::HTTP.get(URI::parse(url))
    render json: res
  end
end
