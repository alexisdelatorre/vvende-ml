require 'net/http'

class MeliOauthTokensController < ApplicationController
  before_action :doorkeeper_authorize!

  def authenticate
    params.permit(:code)

    url_params = {
        :grant_type => 'authorization_code',
        :client_id => ENV['MELI_CLIENT_ID'],
        :client_secret => ENV['MELI_CLIENT_SECRET'],
        :code => params['code'],
        :redirect_uri => ENV['MELI_REDIRECT_URL'],
    }

    res = Net::HTTP.post(URI::parse("https://api.mercadolibre.com/oauth/token?#{url_params.to_query}"), '')
    body = JSON.parse(res.body)

    if res.code == "400"
      render json: { error: body['error'], message: body['message'] }, status: :bad_request
      return
    end

    @meli_oauth_token = current_user.create_meli_oauth_token(
      access_token: body['access_token'],
      expires_in: body['expires_in'],
      scope: body['scope'],
      meli_user_id: body['user_id'],
      refresh_token: body['refresh_token'],
    )

    if @meli_oauth_token.save
      render json: @meli_oauth_token
    else
      render json: @meli_oauth_token.errors, status: :unprocessable_entity
    end
  end

  def disconnect
    current_user.meli_oauth_token.destroy
    render status: 200
  end

  def url
    url = "https://auth.mercadolibre.com.mx/authorization?response_type=code&client_id=#{ENV['MELI_CLIENT_ID']}"
    render plain: url
  end
end


