class ApplicationController < ActionController::API
  wrap_parameters false
  def current_user
    User.find(doorkeeper_token.resource_owner_id)
  end
end
