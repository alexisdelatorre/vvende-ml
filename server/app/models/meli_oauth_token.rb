class MeliOauthToken < ApplicationRecord
  belongs_to :user, optional: true
end
