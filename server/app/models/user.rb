class User < ApplicationRecord
  has_secure_password
  has_one :meli_oauth_token

  def authenticated_meli
    return !self.meli_oauth_token.nil?
  end
end
