Rails.application.routes.draw do
  use_doorkeeper
  get 'users/me', to: 'users#me'
  get 'meli/users/me', to: 'meli_users#me'

  get 'meli/sites', to: 'meli#sites'
  get 'meli/sites/:site_id', to: 'meli#site'
  get 'meli/sites/:site_id/listing_types', to: 'meli#listing_types'
  get 'meli/sites/:site_id/listing_types/:listing_type_id', to: 'meli#listing_type'

  resources :users, except: :index

  post 'oauth/meli/authenticate', to: 'meli_oauth_tokens#authenticate'
  post 'oauth/meli/disconnect', to: 'meli_oauth_tokens#disconnect'
  get 'oauth/meli/url', to: 'meli_oauth_tokens#url'
end
